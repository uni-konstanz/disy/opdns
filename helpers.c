static void opdns_log(const char *format, ...)
{
  static char buf[1024];
  static const char *progname = NULL;
  FILE *log = stderr;
  va_list ap;
  time_t t;
  struct tm tm;

  // Try to obtain command name from /proc once; fall back to "?"
  if (progname == NULL) {
    if (readlink("/proc/self/exe", buf, sizeof(buf) - 1) > 0) {
      buf[sizeof(buf) - 1] = '\0';
      const char *tail = strrchr(buf, '/');
      if (tail == NULL) {
        progname = buf;
      } else {
        progname = tail + 1;
      }
    } else {
      progname = "?";
    }
  }

  t = time(NULL);
  localtime_r(&t, &tm);
  // Try to interfere as little as possible with the user program's fds
  if (opdns_tofile != 0)
    log = fopen(LOGFILE, "a");
  if (log == NULL)
    log = stderr;
  va_start(ap, format);
  // Fall back to stderr on problems
  fprintf(log, "%04d-%02d-%02d %02d:%02d:%02d libopdns.so/%s[%d]: ",
          1900 + tm.tm_year, 1 + tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min,
          tm.tm_sec, progname, getpid());
  vfprintf(log, format, ap);
  va_end(ap);
  if (log != stderr)
    fclose(log);
}

static int opdns_optcopy(char *dst, const char *src, size_t dstlen,
                         size_t srclen)
{
  if (srclen >= dstlen) {
    return 0;
  } else {
    strncpy(dst, src, srclen);
    dst[srclen] = '\0';
    return 1;
  }
}

static char *strlcpy(char *dst, const char *src, size_t len)
{
  strncpy(dst, src, len);
  dst[len - 1] = '\0';
  return dst;
}

static int opdns_a_ipv4(const IP6ADDR *a)
{
  return (a->ints[0] == 0 && a->ints[1] == 0 && a->ints[2] == 0);
}

static char *opdns_name(char *s)
{
  if (s[0] != '[')
    return NULL;
  char *pos = strchr(s, ']');
  if (pos == NULL)
    return NULL;
  int idx = pos - s;
  if (idx > 2 && idx < MAXNAME) {
    s[idx] = '\0';
    return s + 1;
  } else {
    return NULL;
  }
}
