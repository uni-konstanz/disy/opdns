/*
    opDNS: Getting existing software to use
    opportunistic persistent DNS.

    Copyright (C) 2015 Marcel Waldvogel

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#define _GNU_SOURCE
#define __need_res_state
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <dlfcn.h>
#include <openssl/ssl.h>
#include <openssl/x509.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/nameser.h>
#include <resolv.h>
#include "libssl-version.h"
#include "libc-version.h"
#include "libresolv-version.h"
#include "opdns.h"

/* Environment variables used
 * ==========================
 * LD_PRELOAD      used by ld.so, should be set to /full/path/to/opdns.so
 * OPDNS_OPTIONS   comma-separated list of options
 * - debug         be verbose
 * - logfile       log to /var/log/opdns.log instead of stderr
 * - libssl=       full name of libssl.so.*
 * - libc=         full name of libc.so.*
 * - libresolv=    full name of libresolv.so.*
*/

static int opdns_debug = 0, opdns_tofile = 0;
static char opdns_libssl[LIBNAME_MAX] = DEFAULT_LIBSSL,
            opdns_libc[LIBNAME_MAX] = DEFAULT_LIBC,
            opdns_libresolv[LIBNAME_MAX] = DEFAULT_LIBRESOLV;
static int opdns_inited = 0;

CACHEENT opdns_cache[NCACHE], opdns_maybe[NMAYBE];
unsigned int opdns_cnext = 0, opdns_mnext = 0;

// Normally, this would be linked; but I'm trying to expose as few symbols as possible
#include "helpers.c"
#include "cache.c"
#include "mozilla.c"

static void opdns_parse_opts(void)
{
  char *opts, *optend;
  size_t optlen;

  // This is only needed to improve efficiency, not correctness,
  // so an at-least-once semantic is used (increment at the end of the function)
  if (opdns_inited > 0)
    return;

  opts = getenv("OPDNS_OPTIONS");
  if (opts == NULL)
    opts = "";
  /* Non-destructive strtok() clone */
  while (*opts != '\0') {
    optend = index(opts, ',');
    if (optend == NULL) {
      optlen = strlen(opts); // Until EOS
      optend = opts + optlen;
    } else {
      optlen = optend - opts;
      optend++;
    }
    if (strncasecmp(opts, "debug", optlen) == 0) {
      opdns_debug++;
    } else if (strncasecmp(opts, "logfile", optlen) == 0) {
      opdns_tofile++;
    } else if (optlen > 7 && strncasecmp(opts, "libssl=", 7) == 0) {
      if (!opdns_optcopy(opdns_libssl, opts + 7, LIBNAME_MAX, optlen - 7)) {
        ERRORLOG("WARING: Library name for %.*s too long -- ignored\n",
                 (int)optlen, opts);
      }
    } else if (optlen > 5 && strncasecmp(opts, "libc=", 5) == 0) {
      if (!opdns_optcopy(opdns_libc, opts + 5, LIBNAME_MAX, optlen - 5)) {
        ERRORLOG("WARING: Library name for %.*s too long -- ignored\n",
                 (int)optlen, opts);
      }
    } else if (optlen > 10 && strncasecmp(opts, "libresolv=", 10) == 0) {
      if (!opdns_optcopy(opdns_libresolv, opts + 10, LIBNAME_MAX,
                         optlen - 10)) {
        ERRORLOG("WARING: Library name for %.*s too long -- ignored\n",
                 (int)optlen, opts);
      }
    } else {
      ERRORLOG("libopdns.so: WARNING: Unknown option '%.*s' found in "
               "OPDNS_OPTIONS\n",
               (int)optlen, opts);
    }
    opts = optend;
  }

  opdns_inited++;
  opdns_read_cache();
  atexit(opdns_write_cache);
}

// Get a symbol from the original library
static void *opdns_dlsym(const char *name, const char *lib)
{
  if (opdns_inited == 0)
    opdns_parse_opts();
  void *addr = dlsym(RTLD_NEXT, name);
  if (addr == NULL) {
    // Try again with a more specific name
    // Needed for ejabberd and libresolv (and maybe more)
    void *file = dlopen(lib, RTLD_LAZY | RTLD_GLOBAL | RTLD_NOLOAD);
    if (file != NULL) {
      addr = dlsym(file, name);
      if (addr == NULL) {
        ERRORLOG("Cannot find symbol %s in %s\n", name, lib);
      }
      // Can be dlclose()d here. As it wasn't loaded (RTLD_NOLOAD), it won't be
      // unloaded here
      dlclose(file);
    } else {
      ERRORLOG("Cannot find mapped %s looking for symbol %s\n", lib, name);
    }
  }
  return addr;
}


// Used by gethostbyname() and relatives
int __libc_res_nsearch(res_state statp, const char *name, /* domain name */
                       int class, int type, /* class and type of query */
                       u_char *answer,      /* buffer to put answer */
                       int anslen,          /* size of answer */
                       u_char **answerp, u_char **answerp2, int *nanswerp2,
                       int *resplen2, int *answerp2_malloced)
{
  int ansbytes, xtype;
  DNSHDR *d;
  CACHEENT *oc, *om;
  ORIG_FUNC(__libc_res_nsearch, int,
            (res_state statp, const char *name, int class, int type,
             u_char *answer, int anslen, u_char **answerp, u_char **answerp2,
             int *nanswerp2, int *resplen2, int *answerp2_malloced),
            NO_DATA, opdns_libresolv);
  DEBUGLOG("__libc_res_nsearch: name=%s\n", name);
  if (type == 62321) {
    DEBUGLOG("Hacking type\n");
	xtype = ns_t_aaaa; // XXX Hack
  } else {
	xtype = type;
  }
  if (xtype == ns_t_a || xtype == ns_t_aaaa) {
    DEBUGLOG("type=%d, xtype=%d\n", type, xtype);
    /* State machine states
     * ====================
     * 1. In MAYBE: Not yet approved, not in CACHE, query normally
     * 2. Not in MAYBE, in CACHE: Reuse
     * 3. Not in MAYBE, not in CACHE: Query, store in MAYBE
     */
    om = opdns_find(opdns_maybe, NMAYBE, name, xtype);
    if (om == NULL) {
      oc = opdns_find(opdns_cache, NCACHE, name, xtype);
      if (oc != NULL) {
        // 2: om == NULL, oc != NULL
        if (oc->blacklist) { // Don't use cache
          TRACE("__libc_res_nsearch(): %s is blacklisted -- doing DNS lookup\n", name);
          return (*orig___libc_res_nsearch)(
              statp, name, class, type, answer, anslen, answerp, answerp2,
              nanswerp2, resplen2, answerp2_malloced);
        } else { // Use cache
          TRACE("__libc_res_nsearch(): %s in verified cache -- skipping DNS lookup\n", name);
          return opdns_cache_response(oc, type, &answer, anslen);
        }      // if blacklist
      } else { // Not yet in cache or maybe, install in maybe
               // 3: om == NULL, oc == NULL
          TRACE("__libc_res_nsearch(): %s unknown -- doing DNS lookup\n", name);
        ansbytes = (*orig___libc_res_nsearch)(
            statp, name, class, type, answer, anslen, answerp, answerp2,
            nanswerp2, resplen2, answerp2_malloced);
        if (ansbytes > 0) {
          TRACE("__libc_res_nsearch(): %s lookup successful -- adding to watchlist\n", name);
          // add to maybe
          CACHEENT *ce;
          d = (DNSHDR *)answer;
          DEBUGLOG("id=%#x flags=%#x qry=%#x ans=%#x ns=%#x add=%#x\n",
                   ntohs(d->id), ntohs(d->flags), ntohs(d->qdcount),
                   ntohs(d->ancount), ntohs(d->nscount), ntohs(d->arcount));
          ce = opdns_find(opdns_maybe, NMAYBE, name, ns_t_any);
          if (ce == NULL) {
            ce = &opdns_maybe[opdns_mnext];
            // opdns_mnext should never have a value outside [0 .. NMAYBE)
            // ++ and then test is therefore not acceptable
            // (this does not avoid race conditions, but minimizes its impact)
            int mn = opdns_mnext + 1;
            if (mn >= NMAYBE)
              mn = 0;
            opdns_mnext = mn;
            // Clear our new entry
            memset(ce, 0, sizeof(*ce));
          }
          // Merge with potentially existing entry (e.g. IPv4 to IPv6)
          // or duplicate orig___libc_res_nsearch() above in parallel thread
          opdns_fill_cacheent(name, answer, ansbytes, ce);
          return ansbytes;
        }    // if ansbytes > 0
      }      // if oc
    } else { // In maybe; not yet approved for extended use
             // 1: om != NULL (implies oc == NULL)
          TRACE("__libc_res_nsearch(): %s in watchlist -- doing DNS lookup\n", name);
      return (*orig___libc_res_nsearch)(statp, name, class, type, answer,
                                        anslen, answerp, answerp2, nanswerp2,
                                        resplen2, answerp2_malloced);
    } // if om
  }   // if type
  // Not for our cache
  TRACE("__libc_res_nsearch(): Request for %s type %d bypassing opDNS\n", name, type);
  return (*orig___libc_res_nsearch)(statp, name, class, type, answer, anslen,
                                    answerp, answerp2, nanswerp2, resplen2,
                                    answerp2_malloced);
}

int X509_check_host(X509 *x509, const char *name, size_t namelen,
                    unsigned int flags, char **peername)
{
  ORIG_FUNC(X509_check_host, int,
            (X509 * x509, const char *name, size_t namelen, unsigned int flags,
             char **peername),
            0, opdns_libssl);
  return (*orig_X509_check_host)(x509, name, namelen, flags, peername);
}

// XXX hack: This should not be counted as a successful verification
void SSL_set_connect_state(SSL *ssl)
{
  ORIG_FUNC(SSL_set_connect_state, void, (SSL *ssl), , opdns_libssl);
	CACHEENT *ce;
  (*orig_SSL_set_connect_state)(ssl);
	  BIO *bio = SSL_get_rbio(ssl);
		int fd = bio->num;
  
	DEBUGLOG("SSL_set_connect_state(): Looking for fd=%d\n", fd);
		while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, fd)) != NULL) {
	    ce->verified = 1;
	TRACE("SSL_set_connect_state(): assuming verified %s\n", ce->name);

	    int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
	    opdns_cache[cnext++] = *ce;
	    if (cnext >= NCACHE)
	      cnext = 0;
	    opdns_cnext = cnext;

	    ce->valid = 0; // No longer in maybe
    }
}

// XXX hack: This should not be counted as a successful verification
int SSL_connect(SSL *ssl)
{
  ORIG_FUNC(SSL_connect, int, (SSL *ssl), -1, opdns_libssl);
	CACHEENT *ce;
  int retval = (*orig_SSL_connect)(ssl);
  if (retval >= 0) {
	  BIO *bio = SSL_get_rbio(ssl);
		int fd = bio->num;
  
	DEBUGLOG("SSL_connect(): Looking for fd=%d\n", fd);
		while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, fd)) != NULL) {
	    ce->verified = 1;
	TRACE("SSL_connect(): assuming verified %s\n", ce->name);

	    int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
	    opdns_cache[cnext++] = *ce;
	    if (cnext >= NCACHE)
	      cnext = 0;
	    opdns_cnext = cnext;

	    ce->valid = 0; // No longer in maybe
    }
  }
  return retval;
}

long SSL_get_verify_result(const SSL *ssl)
{
  ORIG_FUNC(SSL_get_verify_result, long, (const SSL *ssl), X509_V_ERR_OUT_OF_MEM, opdns_libssl);
	CACHEENT *ce;
	long result = (*orig_SSL_get_verify_result)(ssl);

//	DEBUGLOG("SSL_get_verify_result() -> %ld\n", result);
	if (result == X509_V_OK) {
	  BIO *bio = SSL_get_rbio(ssl);
		int fd = bio->num;
  
	DEBUGLOG("SSL_get_verify_result(): Looking for fd=%d\n", fd);
		while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, fd)) != NULL) {
	    ce->verified = 1;
	TRACE("SSL_get_verify_result(): successfully verified %s\n", ce->name);

	    int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
	    opdns_cache[cnext++] = *ce;
	    if (cnext >= NCACHE)
	      cnext = 0;
	    opdns_cnext = cnext;

	    ce->valid = 0; // No longer in maybe
		}
	}
	return result;
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
  int retval;
  ORIG_FUNC(connect, int,
            (int sockfd, const struct sockaddr *addr, socklen_t addrlen), -1,
            opdns_libc);
  retval = (*orig_connect)(sockfd, addr, addrlen);
  if (retval >= 0 || errno == EINPROGRESS) {
    CACHEENT *ce = opdns_find_addr(opdns_maybe, NMAYBE, addr, addrlen);
    if (ce != NULL) {
      ce->connected = sockfd;
	TRACE("connect(%d) to %s succeeded -- will it become a SSL/TLS connection?\n", sockfd, ce->name);
    } else {
	TRACE("connect(%d) to unwatched remote succeeded -- ignoring\n", sockfd);
    }
  }
  return retval;
}

int close(int fd)
{
	CACHEENT *ce;
  ORIG_FUNC(close, int, (int fd), -1, opdns_libc);
//	DEBUGLOG("close(%d)\n", fd);
	while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, fd)) != NULL) {
    ce->connected = 0;
//	DEBUGLOG("close(%d): %s verified=%d\n", fd, ce->name, ce->verified);
    if (!ce->verified) {
      // The connection was successful but unverified -> blacklist
      TRACE("close(%d): Blacklisting unverified connection to %s\n", fd, ce->name);
      ce->blacklist = 1;

      int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
      opdns_cache[cnext++] = *ce;
      if (cnext >= NCACHE)
        cnext = 0;
      opdns_cnext = cnext;

      ce->valid = 0; // No longer in maybe
    } // if
  } // while
  return (*orig_close)(fd);
}
