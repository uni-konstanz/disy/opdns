#if DOES_NOT_WORK
int ssl_SecureConnect(void *ss, const void *sa) {
  ORIG_FUNC(ssl_SecureConnect, int, (void *, const void *), -1, "libssl3.so");
  return (*ssl_SecureConnect)(ss, sa);
}
#endif


//SSL_IMPORT SECStatus SSL_OptionSet(PRFileDesc *fd, PRInt32 option, PRBool on);
typedef struct PRIOMethods       PRIOMethods;
typedef struct PRFileDesc       PRFileDesc;
typedef struct PRFilePrivate       PRFilePrivate;
typedef struct _MDFileDesc       _MDFileDesc;
typedef union  PRNetAddr        PRNetAddr;
typedef int PRDescIdentity;          /* see: Layering file descriptors */
typedef enum { PR_FAILURE = -1, PR_SUCCESS = 0 } PRStatus;
typedef enum PRDescType
{
    PR_DESC_FILE = 1,
    PR_DESC_SOCKET_TCP = 2,
    PR_DESC_SOCKET_UDP = 3,
    PR_DESC_LAYERED = 4,
    PR_DESC_PIPE = 5
} PRDescType;
typedef enum {
    _PR_TRI_TRUE = 1,
    _PR_TRI_FALSE = 0,
    _PR_TRI_UNKNOWN = -1
} _PRTriStateBool;
typedef enum {
    PR_TRUE = 1,
    PR_FALSE = 0
} PRBool;

struct _MDFileDesc {
    int osfd;
    int tcp_nodelay;  /* used by pt_LinuxSendFile */
};

struct PRFilePrivate {
    uint32_t state;
    PRBool nonblocking;
    _PRTriStateBool inheritable;
    PRFileDesc *next;
    int lockCount;   /*   0: not locked
                         *  -1: a native lockfile call is in progress
                         * > 0: # times the file is locked */
    _MDFileDesc md;
};

struct PRFileDesc {
    const PRIOMethods *methods;         /* the I/O methods table */
    PRFilePrivate *secret;              /* layer dependent data */
    PRFileDesc *lower, *higher;         /* pointers to adjacent layers */
    void (*dtor)(PRFileDesc *fd);
                                        /* A destructor function for layer */
    PRDescIdentity identity;            /* Identity of this particular layer  */
};

union PRNetAddr {
    struct {
        uint16_t family;                /* address family (0x00ff maskable) */
        char data[14];                  /* raw address data */
    } raw;
    struct {
        uint16_t family;                /* address family (AF_INET) */
        uint16_t port;                  /* port number */
        uint32_t ip;                    /* The actual 32 bits of address */
        char pad[8];
    } inet;
    struct {
        uint16_t family;                /* address family (AF_INET6) */
        uint16_t port;                  /* port number */
        uint32_t flowinfo;              /* routing information */
        IP6ADDR ip;                  /* the actual 128 bits of address */
        uint32_t scope_id;              /* set of interfaces for a scope */
    } ipv6;
    struct {                            /* Unix domain socket address */
        uint16_t family;                /* address family (AF_UNIX) */
        char path[104];                 /* null-terminated pathname */
    } local;
};
typedef uint32_t PRIntervalTime;
typedef uint16_t PRInt16;

typedef PRStatus (*PRGetpeernameFN)(PRFileDesc *fd, PRNetAddr *addr);
typedef PRStatus (*PRConnectFN)(PRFileDesc *fd, const PRNetAddr *addr, PRIntervalTime timeout);
typedef PRStatus (*PRConnectcontinueFN)(PRFileDesc *fd, PRInt16 out_flags);


struct PRIOMethods {
    PRDescType file_type;           /* Type of file represented (tos)           */
    void * close;                /* close file and destroy descriptor        */
    void * read;                  /* read up to specified bytes into buffer   */
    void * write;                /* write specified bytes from buffer        */
    void * available;        /* determine number of bytes available      */
    void * available64;    /*          ditto, 64 bit                   */
    void * fsync;                /* flush all buffers to permanent store     */
    void * seek;                  /* position the file to the desired place   */
    void * seek64;              /*           ditto, 64 bit                  */
    void * fileInfo;          /* Get information about an open file       */
    void * fileInfo64;      /*           ditto, 64 bit                  */
    void * writev;              /* Write segments as described by iovector  */
    PRConnectFN connect;            /* Connect to the specified (net) address   */
    void * accept;              /* Accept a connection for a (net) peer     */
    void * bind;                  /* Associate a (net) address with the fd    */
    void * listen;              /* Prepare to listen for (net) connections  */
    void * shutdown;          /* Shutdown a (net) connection              */
    void * recv;                  /* Solicit up the the specified bytes       */
    void * send;                  /* Send all the bytes specified             */
    void * recvfrom;          /* Solicit (net) bytes and report source    */
    void * sendto;              /* Send bytes to (net) address specified    */
    void * poll;                  /* Test the fd to see if it is ready        */
    void * acceptread;      /* Accept and read on a new (net) fd        */
    void * transmitfile;  /* Transmit at entire file                  */
    void * getsockname;    /* Get (net) address associated with fd     */
    PRGetpeernameFN getpeername;    /* Get peer's (net) address                 */
    void * reserved_fn_6;     /* reserved for future use */
    void * reserved_fn_5;     /* reserved for future use */
    void * getsocketoption;
                                    /* Get current setting of specified option  */
    void * setsocketoption;
                                    /* Set value of specified option            */
    void * sendfile;                      /* Send a (partial) file with header/trailer*/
    PRConnectcontinueFN connectcontinue;
                                    /* Continue a nonblocking connect */
    void * reserved_fn_3;         /* reserved for future use */
    void * reserved_fn_2;         /* reserved for future use */
    void * reserved_fn_1;         /* reserved for future use */
    void * reserved_fn_0;         /* reserved for future use */
};

#define SSL_HANDSHAKE_AS_CLIENT         5 /* force accept to hs as client */
#define SSL_ENABLE_TLS                 13 /* enable TLS (on by default) */

PRIOMethods opdns_methods; 
const PRIOMethods *opdns_methods_prev;

static PRStatus opdns_mozilla_ssl_connect_continue(PRFileDesc *fd, PRInt16 out_flags)
{
DEBUGLOG("Mozilla SSL_connect_continue()\n");
  PRStatus retval = (*opdns_methods_prev->connectcontinue)(fd, out_flags); // is intercepted by connect() above
  if (retval == PR_SUCCESS || errno == EINPROGRESS) {
	PRFileDesc *tempfd = fd;
	while (tempfd->lower != NULL) tempfd = tempfd->lower;
	if (tempfd->methods->file_type != PR_DESC_SOCKET_TCP) {
		DEBUGLOG("Mozilla SSL_connect_continue() for file_type=%d\n", tempfd->methods->file_type);
	} else {
		int ifd = tempfd->secret->md.osfd;
		CACHEENT *ce;
	DEBUGLOG("Mozilla SSL_connect_continue(): Looking for fd=%d\n", ifd);
		while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, ifd)) != NULL) {
	    ce->verified = 1;
	TRACE("Mozilla SSL_connect_continue(): assuming verified %s\n", ce->name);

	    int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
	    opdns_cache[cnext++] = *ce;
	    if (cnext >= NCACHE)
	      cnext = 0;
	    opdns_cnext = cnext;

            ce->valid = 0; // No longer in maybe
	}
	}
  }
  return retval;
}
static PRStatus opdns_mozilla_ssl_connect(PRFileDesc *fd, const PRNetAddr *addr, PRIntervalTime timeout)
{
DEBUGLOG("Mozilla SSL_connect()\n");
  PRStatus retval = (*opdns_methods_prev->connect)(fd, addr, timeout); // is intercepted by connect() above
  if (retval == PR_SUCCESS || errno == EINPROGRESS) {
	PRFileDesc *tempfd = fd;
	while (tempfd->lower != NULL) tempfd = tempfd->lower;
	if (tempfd->methods->file_type != PR_DESC_SOCKET_TCP) {
		DEBUGLOG("Mozilla SSL_connect() for file_type=%d\n", tempfd->methods->file_type);
	} else {
		int ifd = tempfd->secret->md.osfd;
		CACHEENT *ce;
	DEBUGLOG("Mozilla SSL_connect(): Looking for fd=%d\n", ifd);
		while ((ce = opdns_find_fd(opdns_maybe, NMAYBE, ifd)) != NULL) {
	    ce->verified = 1;
	TRACE("Mozilla SSL_connect(): assuming verified %s\n", ce->name);

	    int cnext = opdns_cnext; // Pseudo-atomic increment-and-wrap
	    opdns_cache[cnext++] = *ce;
	    if (cnext >= NCACHE)
	      cnext = 0;
	    opdns_cnext = cnext;

            ce->valid = 0; // No longer in maybe
	}
	}
  }
  return retval;
}
PRStatus SSL_OptionSet(PRFileDesc *fd, int option, int on)
{
  ORIG_FUNC(SSL_OptionSet, int, (PRFileDesc *fd, int option, int on), -1, "libssl3.so");
  DEBUGLOG("SSL_OptionSet(%p, %d, %d)\n", fd, option, on);
  // Just hook in
  if (opdns_methods.file_type == 0) {
    opdns_methods = *fd->methods;
    opdns_methods_prev = fd->methods;
    opdns_methods.connect = opdns_mozilla_ssl_connect;
    opdns_methods.connectcontinue = opdns_mozilla_ssl_connect_continue;
  }
  fd->methods = &opdns_methods;
  return (*orig_SSL_OptionSet)(fd, option, on);
}
