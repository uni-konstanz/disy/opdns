#define LOGFILE "/var/log/opdns.log"
#define LIBNAME_MAX 50

#define ERRORLOG(...) opdns_log(__VA_ARGS__)
#define TRACE(...) opdns_log(__VA_ARGS__)
#ifdef NDEBUG
#define DEBUGLOG(...)
#else
#define DEBUGLOG(...) do { if (opdns_debug != 0) opdns_log(__VA_ARGS__); } while (0)
#endif

// opdns_debug, used by DEBUGLOG(), is only valid after the first call to
// opdns_dlsym() (or anything else calling opdns_parse_opts())
#define ORIG_FUNC(func, rettype, args, fail, lib)                              \
  static rettype(*orig_##func) args;                                           \
  if (orig_##func == NULL)                                                     \
    orig_##func = opdns_dlsym(#func, lib);                                     \
  if (orig_##func == NULL)                                                     \
    return fail;

typedef struct {
  uint16_t id, flags, qdcount, ancount, nscount, arcount;
} DNSHDR;

#define NCACHE 100
#define NADDR 10
#define NMAYBE 10
#define MAXNAME 100
#define CACHENAME ".opDNS-cache"
#define MAXLINE 128

typedef union {
  uint8_t bytes[16];
  uint16_t words[8];
  uint32_t ints[4];
} IP6ADDR;

typedef struct {
  unsigned int valid;
  unsigned int connected;
  unsigned int verified;
  unsigned int blacklist;
  char name[MAXNAME];
  unsigned int naddr;
  IP6ADDR addr[NADDR];
} CACHEENT;

static void opdns_log(const char *format, ...)
    __attribute__((format(printf, 1, 2)));
static void *opdns_dlsym(const char *name, const char *lib);
static void opdns_read_cache(void);
static void opdns_write_cache(void);
