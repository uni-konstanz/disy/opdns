static void opdns_read_cache(void)
{
  FILE *f;
  int i;
  char buf[MAXLINE];
  CACHEENT *ce;
  IP6ADDR *a;

  f = fopen(CACHENAME, "r");
  if (f == NULL) {
    DEBUGLOG("Cannot read cache file %s\n", CACHENAME);
    return;
  }
  // Unable to fill the last cache entry
  i = 0;
  ce = NULL;
  while (i < NCACHE && fgets(buf, sizeof(buf), f) != NULL) {
    char *s;
    if (NULL != (s = opdns_name(buf))) {
      if (i == NCACHE - 1)
        break;
      if (ce != NULL) {
        if (ce->blacklist)
          ce->naddr = 0; // Just to be sure
        ce->valid = (ce->naddr > 0) || ce->blacklist;
        if (!ce->valid)
          i--;
      }
      ce = &opdns_cache[i++];
      strlcpy(ce->name, s, MAXNAME);
      ce->naddr = 0;
      a = &ce->addr[ce->naddr];
    } else if (ce != NULL && ce->naddr < NADDR &&
               sscanf(buf, "A %hhu.%hhu.%hhu.%hhu", &a->bytes[12],
                      &a->bytes[13], &a->bytes[14], &a->bytes[15]) == 4) {
      a->ints[0] = a->ints[1] = a->ints[2] = 0;
      a = &ce->addr[++ce->naddr];
    } else if (ce != NULL && ce->naddr < NADDR &&
               sscanf(buf, "AAAA %hx:%hx:%hx:%hx:%hx:%hx:%hx:%hx", &a->words[0],
                      &a->words[1], &a->words[2], &a->words[3], &a->words[4],
                      &a->words[5], &a->words[6], &a->words[7]) == 8) {
      a = &ce->addr[++ce->naddr];
    } else if (ce != NULL && strncmp(buf, "BLACKLIST", 9) == 0) {
      ce->blacklist = 1;
    } else {
      ERRORLOG("Invalid line in cache file %s: %s\n", CACHENAME, buf);
    }
  }
  if (ce != NULL) {
    if (ce->blacklist)
      ce->naddr = 0; // Just to be sure
    ce->valid = (ce->naddr > 0) || ce->blacklist;
  }
  opdns_cnext = i;
  fclose(f);
}

static void opdns_write_cache(void)
{
  FILE *f;
  int i, j;
  CACHEENT *ce;
  IP6ADDR *a;
	char *nif = ""; /* Newline after first */

  f = fopen(CACHENAME, "w");
  if (f == NULL) {
    ERRORLOG("Cannot write cache file %s\n", CACHENAME);
    return;
  }
  // Write MAYBE before CACHE, as it may contain important blacklist information
  // (is this actually true?)
  // (reading is into CACHE only, so the last few entries may be ignored)
  for (i = 0, ce = opdns_maybe; i < NMAYBE; i++, ce++) {
    if (ce->valid && ce->connected && !ce->verified) {
      fprintf(f, "%s[%s]\nBLACKLIST\n", nif, ce->name);
			nif = "\n";
    }
  }
  for (i = 0, ce = opdns_cache; i < NCACHE; i++, ce++) {
    if (ce->valid) {
      fprintf(f, "%s[%s]\n", nif, ce->name);
			nif = "\n";
      if (ce->blacklist) {
        ce->naddr = 0; // Just to be sure
        fprintf(f, "BLACKLIST\n");
      } else {
        for (j = 0, a = ce->addr; j < ce->naddr; j++, a++) {
          if (opdns_a_ipv4(a)) {
            fprintf(f, "A %d.%d.%d.%d\n", a->bytes[12], a->bytes[13],
                    a->bytes[14], a->bytes[15]);
          } else {
            fprintf(f, "AAAA %x:%x:%x:%x:%x:%x:%x:%x\n", a->words[0],
                    a->words[1], a->words[2], a->words[3], a->words[4],
                    a->words[5], a->words[6], a->words[7]);
          }
        } // for j
      }   // else blacklist
    }     // if valid
  }
  fclose(f);
}

static unsigned char *opdns_dncomp(CACHEENT *ce, unsigned char *ptr)
{
  unsigned char *nptr = (unsigned char *)&ce->name;
  while (*nptr != '\0') {
    int len = 0;
    unsigned char *lptr = ptr;
    ptr++; // Skip length
    while (*nptr != '\0' && *nptr != '.') {
      len++;
      *ptr++ = *nptr++;
    }
    // Fixup length
    if (len == 0 || len > 63) {
      ERRORLOG("Illegal length %u at %u in %s -- ABORT\n", len,
               (unsigned int)(nptr - (unsigned char *)&ce->name), ce->name);
      exit(99);
    }
    *lptr = len;
    // Skip over any dots (should be at most one...)
    while (*nptr == '.') nptr++;
  }
  *ptr++ = '\0'; // Last element
  return ptr;
}

static int opdns_of_type(CACHEENT *ce, int type)
{
  int count = 0;
  // IPv6?
  if (type == ns_t_aaaa)
    return ce->naddr;

  // IPv4?
  for (int i = 0; i < ce->naddr; i++) {
    count += opdns_a_ipv4(&ce->addr[i]);
  }
  return count;
}

// reference casts "(unsigned char *&)" do not seem to work
#define PUTB(ptr, x) (*((*(unsigned char  **)&(ptr))++) = (x))
#define PUTW(ptr, x) (*((*(unsigned short **)&(ptr))++) = htons(x))
#define PUTL(ptr, x) (*((*(unsigned int   **)&(ptr))++) = htonl(x))
static int opdns_cache_response(CACHEENT *ce, int type, unsigned char **pktbuf,
                                size_t pktbufsize)
{
  unsigned char
      pkt[sizeof(DNSHDR) + 5 + MAXNAME + (NS_IN6ADDRSZ + 12) * NADDR + 100];
  unsigned char *ptr = pkt;
  int nreplies = opdns_of_type(ce, type);

  // Header
  PUTW(ptr, 0);        // ID
  PUTW(ptr, 0x8180);   // Successful recursive answer
  PUTW(ptr, 1);        // #Questions
  PUTW(ptr, nreplies); // #Replies
  PUTW(ptr, 0);        // #Authoritative
  PUTW(ptr, 0);        // #Additional

  // Query
  ptr = opdns_dncomp(ce, ptr); // QNAME
  PUTW(ptr, type);             // QTYPE
  PUTW(ptr, ns_c_in);          // QCLASS

  // Answer(s)
  for (int i = 0; i < ce->naddr; i++) {
    if (type == ns_t_aaaa || (type == ns_t_a && opdns_a_ipv4(&ce->addr[i]))) {
      PUTW(ptr, 0xc00c);  // NAME as pointer to QNAME
      PUTW(ptr, type);    // TYPE
      PUTW(ptr, ns_c_in); // CLASS
      PUTL(ptr, 60);      // TTL
      if (type == ns_t_aaaa) {
        PUTW(ptr, NS_IN6ADDRSZ);                 // RDLENGTH
        memcpy(ptr, &ce->addr[i], NS_IN6ADDRSZ); // RDATA
        ptr += NS_IN6ADDRSZ;
      } else {
        PUTW(ptr, NS_INADDRSZ);         // RDLENGTH
				// PUTL() would do another htonl() which we do not need here
        memcpy(ptr, &ce->addr[i].ints[3], NS_INADDRSZ); // RDATA
        ptr += NS_INADDRSZ;
      }
    }
  }
  size_t len = ptr - pkt;
  if (*pktbuf == NULL) {
    *pktbuf = malloc(len);
    if (*pktbuf == NULL) {
      ERRORLOG("Could not malloc %zu bytes -- ABORT\n", len);
      exit(99);
    }
    memcpy(*pktbuf, pkt, len);
  } else {
    if (pktbufsize > len) {
      memcpy(*pktbuf, pkt, len);
    } else {
      ERRORLOG(
          "Provided buffer of size %zu not large enough for %zu -- ABORT\n",
          pktbufsize, len);
      exit(99);
    }
  }
  return len;
}

static int opdns_fill_cacheent(const char *name, const u_char *answer,
                               int anslen, CACHEENT *ce)
{
  ns_msg msg;
  ns_rr rr;
//  char dispbuf[100];
  int i, l;

  strlcpy(ce->name, name, sizeof(ce->name));
  ns_initparse(answer, anslen, &msg);
  l = ns_msg_count(msg, ns_s_an);
  for (i = 0; i < l && ce->naddr < NADDR; i++) {
    IP6ADDR *a = &ce->addr[ce->naddr];
    //		ce->naddr = 0;
    ns_parserr(&msg, ns_s_an, i, &rr);
//    ns_sprintrr(&msg, &rr, NULL, NULL, dispbuf, sizeof(dispbuf));
//    printf("\t%s \n", dispbuf);
    if (ns_rr_type(rr) == ns_t_a && rr.rdlength == 4) {
      a->ints[0] = a->ints[1] = a->ints[2] = 0;
      memcpy(&a->ints[3], rr.rdata, 4);
      ce->naddr++;
    } else if (ns_rr_type(rr) == ns_t_aaaa && rr.rdlength == 16) {
      memcpy(&a->ints[3], rr.rdata, 16);
      ce->naddr++;
    }
  }
  // Clear remaining addresses and fields; ce->naddr < i for CNAMEs etc.
  memset(&ce->addr[ce->naddr], 0, (NADDR - ce->naddr) * sizeof(&ce->addr[i]));
  ce->blacklist = ce->connected = ce->verified = 0;

  ce->valid = (ce->naddr > 0);
  return ce->valid;
}

static CACHEENT *opdns_find(CACHEENT *ce, int count, const char *name,
                            int type)
{
  for (int i = 0; i < count; i++, ce++) {
    if (ce->valid && strcmp(ce->name, name) == 0) {
      if (type == ns_t_any || opdns_of_type(ce, type) > 0 || ce->blacklist) {
        return ce;
      } else {
        return NULL; // There can be no other with this name
      }
    }
  }
  return NULL;
}

static CACHEENT *opdns_find_addr(CACHEENT *ce, int count,
                                 const struct sockaddr *addr,
                                 socklen_t addrlen)
{
  for (int i = 0; i < count; i++, ce++) {
    if (ce->valid) {
      IP6ADDR *a = &ce->addr[0];
      if (addr->sa_family == AF_INET) {
        for (int j = 0; j < ce->naddr; j++, a++) {
          if (opdns_a_ipv4(a) &&
              memcmp(&a->ints[3], &((const struct sockaddr_in *)addr)->sin_addr, NS_INADDRSZ) == 0) {
            return ce;
          }
        }
      } else if (addr->sa_family == AF_INET6) {
        for (int j = 0; j < ce->naddr; j++, a++) {
          if (memcmp(a, &((const struct sockaddr_in6 *)addr)->sin6_addr,
                     NS_IN6ADDRSZ) == 0) {
            return ce;
          }
        }
      }
    }
  }
  return NULL;
}
static CACHEENT *opdns_find_fd(CACHEENT *ce, int count,
                               int fd)
{
  for (int i = 0; i < count; i++, ce++) {
    if (ce->valid && ce->connected == fd) {
			return ce;
    }
  }
  return NULL;
}
